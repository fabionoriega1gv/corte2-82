import express  from 'express';
import  json  from 'body-parser';
import alumnosDb from '../models/alumnos.js';

export const router = express.Router();

// Declarar primer ruta por omisión

router.get('/', (req,res)=>{
    res.render('index',{titulo:"Mis Practicas JS",nombre:"Fabio Manuel Noriega Fitch"});
})

router.get('/tabla',(req,res)=>{
    // Paramétros
    const params = {
        numero:req.query.numero
    }
    res.render('tabla',params);
})

router.post('/tabla', (req,res)=>{
    const numero = req.body.numero;
    if(req.body.limpiar){
        res.render('tabla',{ numero:null });
    } else {
        res.render('tabla',{numero});
    }
})
router.get('/cotizacion', (req, res) => {
    const params = {
        folio: req.query.folio,
        desc: req.query.desc,
        valor: req.query.valor,
        pinicial: req.query.pinicial,
        plazos: req.query.plazos
    };
    res.render('cotizacion', params);
});

router.post('/cotizacion', (req, res) => {
    const params = {
        folio: req.body.folio,
        desc: req.body.desc,
        valor: req.body.valor,
        pinicial: req.body.pinicial,
        plazos: req.body.plazos
    };
    res.render('cotizacion', params);
});

/*router.get('/alumnos',(reg,res) =>{
    const params ={
        
    }
    res.render('alumnos',params)
})
*/
let params;
router.post('/alumnos',async(req,res)=>{
    try{
     params={
        matricula:req.body.matricula,
        nombre:req.body.nombre,
        domicilio:req.body.domicilio,
        sexo:req.body.sexo,
        especialidad:req.body.especialidad   
     }
    const res = await alumnosDb.insertar(params);

    } catch(error){
        console.error(error)
        res.status(400).send("sucedio un error:" + error);
    }
    rows = await alumnosDb.mostrarTodos();
    res.render('alumnos',{reg:rows});
});

let rows; 
    router.get('/alumnos',async(reg,res)=>{
    rows = await alumnosDb.mostrarTodos();
    res.render('alumnos',{reg:rows});

})

export default {router}